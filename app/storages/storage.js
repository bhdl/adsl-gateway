'use strict'

import getConfig from '../config/get.config'
import ConfigtypeEnum from '../config/configtype.enum'
import debug from '../core/loggers/debug'
import LocalStorage from './local.storage.js'
import MongoGatewayStorage from './mongo.gateway.storage'

class Storage {
  constructor (namespace) {
    this.mongoStorage = new MongoGatewayStorage(namespace)
    this.mapStorage = new LocalStorage()
    this.keyName = 'packageId'
    this.bufferSize = getConfig('BUFFER_SIZE', ConfigtypeEnum.INT)
  }

  store (valueObject) {
    if (this.mapStorage.size === this.bufferSize) {
      debug.dev(`Storage: Buffer reached limit of ${this.bufferSize}`)

      let messages = this.mapStorage.toArray()
      debug.dev(`Storage: Buffer content staged`)

      this.mapStorage.clear()
      debug.dev(`Storage: Buffer cleared`)

      this.persistentStore(messages)
      debug.dev('Staged content of buffer forwarded to Mongo')
    }

    this.mapStorage.set(valueObject[this.keyName], valueObject)
    debug.dev('Storage: Message is stored in memory')
  }

  persistentStore (messages) {
    this.mongoStorage.insertMany(messages)
      .catch((error, result) => {
        for (let failed of result.ops) {
          this.store(failed)
        }
        return error
      })
  }

  delete (packageId) {
    if (this.mapStorage.has(packageId)) {
      this.mapStorage.delete(packageId)
      debug.dev(`Storage: Message deleted from memory (packageId: ${packageId}`)
    } else {
      this.mongoStorage.delete(packageId)
    }
  }

  getAll (callback) {
    // send back from the map storage
    callback(this.mapStorage.toArray())

    // send back from mongo
    this.mongoStorage.getSome().then((result) => {
      callback(result)
    })
  }
}

module.exports = Storage
