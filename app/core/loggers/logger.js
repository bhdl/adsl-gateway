'use strict'

import path from 'path'

import winston from 'winston'

import config from '../../config'

const options = {
  file: {
    level: 'info',
    filename: path.resolve(config.rootDir, './logs/app.log'),
    handleExceptions: true,
    json: true,
    format: winston.format.json(),
    maxsize: 5242880, // 5MB
    maxFiles: 10,
    colorize: false
  }
}

class Logger {
  constructor () {
    this.logger = winston.createLogger({
      transports: [
        new winston.transports.File(options.file)
      ],
      exitOnError: false
    })
  }

  debug (msg, data) {
    this._log('debug', msg, data)
  }

  info (msg, data) {
    this._log('info', msg, data)
  }

  notice (msg, data) {
    this._log('notice', msg, data)
  }

  warning (msg, data) {
    this._log('warning', msg, data)
  }

  error (msg, data) {
    this._log('error', msg, data)
  }

  _log (level, msg, data) {
    this.logger.log({level: level, msg: msg, data: data})
  }
}

export default new Logger()
