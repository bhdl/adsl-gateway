'use strict'

import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import time from '../../../core/time'
import ipsData from '../../../data/server/ips'
import logger from '../../../core/loggers/logger'
import debug from '../../../core/loggers/debug'
import ipsClientController from '../../client/controllers/ips.client.controller'
import IpsSuccessResponse from '../../../core/responses/ips.success.response'
import ipsStorage from '../../../storages/ips'

function ipsEventHandler (socket, namespace) {
  return (data) => {
    debug.dev('Server: Data received', data)
    const currentTimestamp = time.currentTimeInMillis()
    const timestampOffset = currentTimestamp - data.timestamp
    logger.info('incoming-ips-data', data)
    const errorResponse = ipsData.validate(data)

    if (errorResponse) {
      debug.warn(errorResponse.getResponseData())
      socket.emit('response', errorResponse.getResponseData())
      return
    }

    const syncronizedData = time.syncronizeTimestamp(data, timestampOffset)

    ipsStorage.store(syncronizedData)

    ipsClientController.send(syncronizedData)

    socket.emit('response', new IpsSuccessResponse(data.packageId, data).getResponseData())
  }
}
export default new BaseEventHandler('message', ipsEventHandler)
