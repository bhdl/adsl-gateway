'use strict'

import Joi from 'joi'

import Validator from '../../core/models/validator'

import { formatConfig } from '../../config'

const commonSchema = {
  status: Joi.string().required(),
  code: Joi.number().integer().required(),
  message: Joi.string().required(),
  packageId: Joi.string().regex(formatConfig.packageId),
  data: Joi.any()
}

const clientSchema = Joi.object({
  ...commonSchema
})

export default new Validator(clientSchema)
