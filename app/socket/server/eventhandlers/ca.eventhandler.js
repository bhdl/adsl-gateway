'use strict'

import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import { transformCaData } from '../../../models/ca'
import caData from '../../../data/server/ca'
import logger from '../../../core/loggers/logger'
import IpsSuccessResponse from '../../../core/responses/ips.success.response'

function caEventHandler (socket, namespace) {
  return (data) => {
    logger.info('incoming-ca-data', data)
    const errorResponse = caData.validate(data)

    if (errorResponse) {
      socket.emit('response', errorResponse.getResponseData())
      logger.error('Incoming CA data is invalid', data)
      return
    }

    var documentList = transformCaData(data)

    for (var ca of documentList) {
      logger.info(`Emit CA event: ${ca.event}`, ca.document)
      socket.broadcast.emit(ca.event, ca.document)
    }

    socket.emit('response', new IpsSuccessResponse(data.packageId, data).getResponseData())
  }
}

export default new BaseEventHandler('ca-emergency', caEventHandler)
