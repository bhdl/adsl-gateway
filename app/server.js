'use strict'

import { Server } from 'http'

function createServer (expressApp) {
  const server = Server(expressApp)
  return server
}

export default createServer
