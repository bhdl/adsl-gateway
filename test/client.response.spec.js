'use strict'

import { expect } from 'chai'

import clientResponseData from '../app/data/client/client.response'

describe('IPS model transform', function () {
  it('should be correct', function () {
    const testData = {
      'status': 'OK',
      'code': 200,
      'message': 'The sended data is proccessed',
      'packageId': 'ips01_2018_09_11_14_47_40_246',
      'data': {
        'timestamp': 1536670060261,
        'clientId': 'ips01',
        'packageId': 'ips01_2018_09_11_14_47_40_246',
        'type': 'rfid',
        'data': {
          'readerStatus': {
            'temperature': 49
          },
          'rfData': [
            {
              'tagId': 'rf0',
              'antennaId': 'ann067',
              'rssi': 37,
              'timestamp': 1536669767611
            },
            {
              'tagId': 'rf1',
              'antennaId': 'ann011',
              'rssi': 27,
              'timestamp': 1536669851493
            },
            {
              'tagId': 'rf2',
              'antennaId': 'ann011',
              'rssi': 18,
              'timestamp': 1536669892934
            },
            {
              'tagId': 'rf3',
              'antennaId': 'ann073',
              'rssi': 90,
              'timestamp': 1536669796441
            },
            {
              'tagId': 'rf4',
              'antennaId': 'ann089',
              'rssi': 36,
              'timestamp': 1536669850571
            },
            {
              'tagId': 'rf5',
              'antennaId': 'ann094',
              'rssi': 68,
              'timestamp': 1536669897624
            },
            {
              'tagId': 'rf6',
              'antennaId': 'ann038',
              'rssi': 78,
              'timestamp': 1536669847105
            },
            {
              'tagId': 'rf7',
              'antennaId': 'ann003',
              'rssi': 55,
              'timestamp': 1536670033360
            },
            {
              'tagId': 'rf8',
              'antennaId': 'ann037',
              'rssi': 14,
              'timestamp': 1536669765374
            },
            {
              'tagId': 'rf9',
              'antennaId': 'ann066',
              'rssi': 23,
              'timestamp': 1536670042539
            },
            {
              'tagId': 'rf10',
              'antennaId': 'ann064',
              'rssi': 47,
              'timestamp': 1536669898603
            },
            {
              'tagId': 'rf11',
              'antennaId': 'ann042',
              'rssi': 61,
              'timestamp': 1536669993954
            },
            {
              'tagId': 'rf12',
              'antennaId': 'ann045',
              'rssi': 6,
              'timestamp': 1536669898792
            },
            {
              'tagId': 'rf13',
              'antennaId': 'ann058',
              'rssi': 51,
              'timestamp': 1536669845035
            },
            {
              'tagId': 'rf14',
              'antennaId': 'ann092',
              'rssi': 62,
              'timestamp': 1536669957601
            }
          ]
        }
      }
    }

    const errorResponse = clientResponseData.validate(testData)
    expect(errorResponse).to.be.equal(undefined)
  })
})
