// @flow

'use strict'

import debug from '../../loggers/debug'
import BaseEventHandler from './base.eventhandler'

/**
 * Connect Event Handler for Socket.io Client
 * @param {Object} socket
 * @param {Object} namespace
 */
function baseConnectEventHandler (socket: Object, namespace?: Object) {
  return () => {
    debug.dev(`Connected to remote server on "${namespace}"`)
  }
}

export default new BaseEventHandler('connect', baseConnectEventHandler)
