// @flow

'use strict'

import BaseController from '../../../core/socket/server/base.controller'
import caEventHandler from '../eventhandlers/ca.eventhandler'

const route = 'collision-avoidance'

const middlewares = []

const eventHandlers = [caEventHandler]

export default new BaseController(route, middlewares, eventHandlers)
