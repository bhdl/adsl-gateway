'use strict'

import { expect } from 'chai'

import EventHandlerList from '../app/core/socket/eventhandler/eventhandler.list'

describe('EventHandlerList', function () {
  describe('iterator', function () {
    it('could be iterated', function () {
      const iterator = (new EventHandlerList())[Symbol.iterator]
      expect(iterator).not.to.be.an('undefined')
    })
  })
})
