// @flow

'use strict'

import BaseClientController from '../../../core/socket/client/base.client.controller'
import rfidEventHandler from '../eventhandlers/rfid.response.client.eventhandler'
import rfidConnectEventHandler from '../eventhandlers/rfid.connect.client.eventhandler'
import debug from '../../../core/loggers/debug'

const route = 'gateways/srth-1/rfid'

const middlewares = []

const eventHandlers = [rfidEventHandler, rfidConnectEventHandler]

const rfidClientController = new BaseClientController(route, middlewares, eventHandlers)

rfidClientController.on('messageSent', (message) => {
  debug.info(`RFID Message sent to Cloud ${message.packageId}`)
})

export default rfidClientController
