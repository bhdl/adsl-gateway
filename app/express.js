'use strict'

import express from 'express'

import Raven from './core/loggers/sentry'

function createExpress () {
  const app = express()
  app.use(Raven.requestHandler())
  app.use(Raven.errorHandler())
  return app
}

export default createExpress
