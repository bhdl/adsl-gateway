'use strict'

import db from '../db'
import Raven from '../core/loggers/sentry'
import log from '../core/loggers/logger'

export default class BaseStorage {
  constructor (collectionName) {
    this._collection = collectionName
    this._db = db
  }

  async insertMany (documents) {
    return new Promise((resolve, reject) => {
      this._db.getConnection()
        .then(connection => { return this._insertMany(connection, documents) })
        .then(result => { resolve(result) })
        .catch((error, result) => { reject(error, result) })
    })
  }

  _insertMany (connection, documents) {
    return new Promise((resolve, reject) => {
      connection.collection(this._collection)
        .insertMany(documents, (error, result) => {
          if (error) {
            Raven.captureException(error)
            return reject(error, result)
          }

          return resolve(result)
        })
    })
  }

  async delete (packageId) {
    return new Promise((resolve, reject) => {
      this._db.getConnection()
        .then(connection => {
          this._delete(connection, packageId).then(result => {
            resolve(result)
          })
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  _delete (connection, packageId) {
    return new Promise((resolve, reject) => {
      connection.collection(this._collection)
        .deleteOne({ packageId: packageId })
        .then(result => {
          if (result.result.n) {
            log.info(`Mongo: Document deleted (packageId: ${packageId})`)
          } else {
            log.info(`Mongo: Delete has no effect (packageId: ${packageId}`)
          }
          resolve(result)
        })
        .catch(error => {
          log.error(`Mongo: Error`, error)
          reject(error)
        })
    })
  }
}
