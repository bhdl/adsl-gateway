'use strict'

import { expect } from 'chai'

import { transformCaData } from '../app/models/ca'

describe('CA model transform', function () {
  it('should be correct', function () {
    const testData = {
      timestamp: 1535459539539,
      clientId: 'ips01',
      type: 'position',
      gatewayId: 'srth-1',
      packageId: 'ips01_2018_08_21_10_43_21_123',
      data: {
        emergencies: [
          {
            distance: 2.08,
            accuracy: 0.05,
            sharers: ['tag001', 'tag002']
          },
          {
            distance: 2.38,
            accuracy: 0.15,
            sharers: ['tag003', 'tag004']
          },
          {
            distance: 2.43,
            accuracy: 0.25,
            sharers: ['tag005', 'tag006']
          },
          {
            distance: 2.18,
            accuracy: 0.25,
            sharers: ['tag007', 'tag008']
          }
        ]
      }
    }

    const transformData = transformCaData(testData)
    for (var i in transformData) {
      i = +i
      const expectedData = testData.data.emergencies[i + 1 > transformData.length / 2 ? i - transformData.length / 2 : i]

      expect(transformData[i].event).to.be.equal(`ca-emergency-${expectedData.sharers[i + 1 > transformData.length / 2 ? 0 : 1]}`)
      expect(transformData[i].document.packageId).to.be.equal(testData.packageId)
      expect(transformData[i].document.gatewayId).to.be.equal(testData.gatewayId)
      expect(transformData[i].document.clientId).to.be.equal(testData.clientId)
      expect(transformData[i].document.timestamp).to.be.equal(testData.timestamp)

      expect(transformData[i].document.data.emergency.sharer).to.be.equal(expectedData.sharers[i + 1 > transformData.length / 2 ? 1 : 0])
      expect(transformData[i].document.data.emergency.distance).to.be.equal(expectedData.distance)
      expect(transformData[i].document.data.emergency.accuracy).to.be.equal(expectedData.accuracy)
    }
  })
})
