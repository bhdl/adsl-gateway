'use strict'

import { MongoClient } from 'mongodb'

import debug from '../loggers/debug'

export default class MongoDB {
  constructor (uri, database) {
    this._uri = uri
    this._database = database
    this._connection = null
  }

  getConnection () {
    return new Promise((resolve, reject) => {
      if (this._connection && this._connection.serverConfig.isConnected()) {
        return resolve(this._connection)
      }

      MongoClient.connect(this._uri, { useNewUrlParser: true }, (err, client) => {
        if (err) {
          return reject(err)
        }

        debug.info('Connected to Mongo database')
        this._connection = client.db(this._database)
        return resolve(this._connection)
      })
    })
  }
}
