'use strict'

import path from 'path'
import dotenv from 'dotenv'
import ConfigtypeEnum from './configtype.enum'
import getConfig from './get.config'

const relPathToProjectRoot = '../../'
const dotenvFilePath = path.resolve(__dirname, relPathToProjectRoot, '.env')
const { error } = dotenv.config({path: dotenvFilePath})

if (error) {
  throw error
}

export default {
  appName: getConfig('APP_NAME'),
  port: getConfig('APP_PORT'),
  rootDir: path.resolve(__dirname, relPathToProjectRoot),
  nodeEnv: process.env.NODE_ENV || 'DEV'
}

export const dbConfig = {
  host: getConfig('MONGO_HOST'),
  port: getConfig('MONGO_PORT', ConfigtypeEnum.INT),
  database: getConfig('MONGO_DB'),
  username: getConfig('MONGO_USER'),
  password: getConfig('MONGO_PASSWORD'),
  options: {
    autSource: getConfig('MONGO_DB'),
    authMechanism: 'SCRAM-SHA-1'
  }
}

export const formatConfig = {
  packageId: /\w*_\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}_\d{3}/
}

export const sentryConfig = {
  key: getConfig('SENTRY_KEY'),
  project: getConfig('SENTRY_PROJECT')
}

export const cloudConfig = {
  host: getConfig('CLOUD_HOST')
}
