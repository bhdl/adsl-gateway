'use strict'

import Raven from 'raven'

import config, { sentryConfig } from '../../config'

const sentryUrl = `https://${sentryConfig.key}@sentry.io/${sentryConfig.project}`

Raven.config(sentryUrl, { environment: config.nodeEnv }).install()

export default Raven
