// @flow

'use strict'

import BaseController from '../../../core/socket/server/base.controller'
import ipsEventHandler from '../eventhandlers/ips.eventhandler'

const route = 'ips'

const middlewares = []

const eventHandlers = [ipsEventHandler]

export default new BaseController(route, middlewares, eventHandlers)
