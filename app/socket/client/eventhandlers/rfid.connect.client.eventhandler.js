// @flow

'use strict'

import debug from '../../../core/loggers/debug'
import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import rfidStorage from '../../../storages/rfid'
import rfidClient from '../controllers/rfid.client.controller'

/**
 * Connect Event Handler for Socket.io Client
 * @param {Object} socket
 * @param {Object} namespace
 */
function rfidConnectEventHandler (socket: Object, namespace?: Object) {
  return () => {
    let sendStoredMessages = () => {
      rfidStorage.getAll(failedMessages => {
        if (!failedMessages || !failedMessages.length) {
          return
        }

        rfidClient.sendAll(failedMessages)
        debug.dev(`Client: connected status is ${socket.connected}`)
        if (socket.connected && failedMessages.length === parseInt(process.env.MONGO_RETRIEVE_LIMIT)) {
          setTimeout(() => {
            sendStoredMessages()
          }, 1000)
        }
      })
    }

    sendStoredMessages()
  }
}

export default new BaseEventHandler('connect', rfidConnectEventHandler)
