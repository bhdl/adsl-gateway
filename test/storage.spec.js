'use strict'

import Mongo from '../app/storages/mongo.gateway.storage'
import { expect } from 'chai'

const mongo = new Mongo('ips_teszt')

describe('Mongo Storage', function () {
  it('should be correct', function (done) {
    mongo.insertMany([
      {
        testData: {
          testKey: 'testValue'
        }
      },
      {
        testData: {
          testKey: 'testValue2'
        }
      }
    ]).then(result => {
      expect(result)
      done()
    })
  })
})
