// @flow

'use strict'

import BaseEventHandler from '../eventhandler/base.eventhandler'
import EventHandlerList from '../eventhandler/eventhandler.list'
import type { middlewareType } from './base.middleware'
import baseErrorEventHandler from '../eventhandler/base.error.eventhandler'
import baseConnectEventHandler from '../eventhandler/base.connect.eventhandler'
import baseDisconnectingEventHandler from '../eventhandler/base.disconnect.eventhandler'
import baseDisconnectEventHandler from '../eventhandler/base.disconnecting.eventhandler'
import ioClient from 'socket.io-client'
import { cloudConfig } from '../../../config'
import EventEmitter from 'events'
import debug from '../../loggers/debug'

export type routeType = string | RegExp

export default class BaseClientController extends EventEmitter {
  _route: routeType
  _eventHandlers: EventHandlerList
  _middlewares: Array<middlewareType>
  _socket: ioClient

  constructor (route: routeType, middlewares: Array<middlewareType>, eventHandlers: Array<BaseEventHandler>) {
    super()
    this.route = route
    this.middlewares = middlewares
    this.eventHandlers = eventHandlers
  }

  set route (route: routeType): void {
    this._route = route
  }

  get route (): routeType {
    return this._route
  }

  set socket (socket: ioClient): void {
    this._socket = socket
  }

  get socket (): ioClient {
    return this._socket
  }

  set eventHandlers (eventhandlers: Array<BaseEventHandler>): void {
    this._eventHandlers = new EventHandlerList(eventhandlers)
  }

  get eventHandlers (): EventHandlerList {
    return this._eventHandlers
  }

  set middlewares (middlewares: Array<middlewareType>) {
    this._middlewares = []
    for (const middleware of middlewares) {
      this._middlewares.push(middleware)
    }
  }

  get middlewares (): Array<middlewareType> {
    return this._middlewares
  }

  send (message) {
    if (!this._socket.disconnected) {
      message.gatewayId = 'srth-1'
      this._socket.emit('message', message)
      // inner event
      this.emit('messageSent', message)
    }
  }

  sendAll (messages) {
    for (let message of messages) {
      this.send(message)
    }
  }

  static initController (controller: BaseClientController): void {
    const namespace = controller.route
    const ioUrl = `${cloudConfig.host}/${namespace}`
    debug.info(ioUrl)
    controller.socket = ioClient(ioUrl, {transports: ['websocket', 'polling']})

    BaseClientController.applyEventHandlers(controller.socket, namespace, controller.eventHandlers)

    BaseClientController.applyMiddlewares(namespace, controller.middlewares)
  }

  static applyMiddlewares (namespace: Object, middlewares: Array<middlewareType>): void {
    for (const middleware of middlewares) {
      namespace.use(middleware)
    }
  }

  static applyEventHandlers (socket: Object, namespace: Object, eventHandlers: EventHandlerList) {
    for (const eventHandlerData of eventHandlers) {
      const eventHandler = eventHandlerData[1]
      socket.on(eventHandler.eventName, eventHandler.initEventHandler(socket, namespace))
    }

    const defaultEventHandlers = [baseConnectEventHandler, baseErrorEventHandler, baseDisconnectingEventHandler, baseDisconnectEventHandler]
    for (const eventHandler of defaultEventHandlers) {
      if (!eventHandlers.hasEventHandler(eventHandler.eventName)) {
        socket.on(eventHandler.eventName, eventHandler.initEventHandler(socket, namespace))
      }
    }
  }
}
