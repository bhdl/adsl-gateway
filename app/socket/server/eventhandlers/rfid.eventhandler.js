// @flow

'use strict'

import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import time from '../../../core/time'
import rfidData from '../../../data/server/rfid'
import logger from '../../../core/loggers/logger'
import debug from '../../../core/loggers/debug'
import rfidClientController from '../../client/controllers/rfid.client.controller'
import RfidSuccessResponse from '../../../core/responses/rfid.success.response'
import rfidStorage from '../../../storages/rfid'

function rfidEventHandler (socket: Object, namespace?: Object) {
  return (data) => {
    const currentTimestamp = time.currentTimeInMillis()
    const timestampOffset = currentTimestamp - data.timestamp
    logger.info('incoming-rfid-data', data)
    const errorResponse = rfidData.validate(data)

    if (errorResponse) {
      debug.warn(errorResponse.getResponseData())
      socket.emit('response', errorResponse.getResponseData())
      return
    }

    const syncronizedData = time.syncronizeTimestamp(data, timestampOffset)

    rfidStorage.store(syncronizedData)

    rfidClientController.send(syncronizedData)

    socket.emit('response', new RfidSuccessResponse(data.packageId, data).getResponseData())

    /*
    rfidStorage.insertMany(documentList)
      .then(() => { socket.emit('response', new RfidSuccessResponse(data.packageId, data).getResponseData()) })
      .catch((err) => { socket.emit('response', new MongoErrorResponse(data.packageId, err).getResponseData()) })
      */
  }
}

export default new BaseEventHandler('message', rfidEventHandler)
