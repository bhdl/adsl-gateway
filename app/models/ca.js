'use strict'

import getConfig from '../config/get.config'

function transformCaData (caDataPackage) {
  let documentList = []

  let commonData = {
    packageId: caDataPackage.packageId,
    timestamp: caDataPackage.timestamp,
    gatewayId: getConfig('GATEWAY_ID'),
    clientId: caDataPackage.clientId,
    type: 'collision-avoidance'
  }

  for (const emergency of caDataPackage.data.emergencies) {
    let document = { ...commonData }

    document.data = {
      emergency: {
        distance: emergency.distance,
        accuracy: emergency.accuracy,
        sharer: emergency.sharers[0]
      }
    }
    documentList.push({ event: `ca-emergency-${emergency.sharers[1]}`, document: document })
  }

  for (const emergency of caDataPackage.data.emergencies) {
    let document = { ...commonData }

    document.data = {
      emergency: {
        distance: emergency.distance,
        accuracy: emergency.accuracy,
        sharer: emergency.sharers[1]
      }
    }
    documentList.push({ event: `ca-emergency-${emergency.sharers[0]}`, document: document })
  }

  return documentList
}

export { transformCaData }
