// @flow

'use strict'

import Joi from 'joi'

import { formatConfig } from '../../config'
import ValidationErrorResponse from '../responses/validation.error.response'

class Validator {
  schema: Joi.object

  constructor (schema: Joi.object) {
    this.schema = schema
  }

  validate (value: Object): ?ValidationErrorResponse {
    const { error } = Joi.validate(value, this.schema, { abortEarly: false })
    if (error) {
      const isValidPackageId = formatConfig.packageId.test(value.packageId)
      const packageId = isValidPackageId ? value.packageId : 'UNKNOWN_0000_00_00_00_00_00_000'
      const data = { errors: error.details.map((err) => { return { path: err.path, message: err.message } }) }
      return new ValidationErrorResponse(packageId, data)
    }
  }
}

export default Validator
