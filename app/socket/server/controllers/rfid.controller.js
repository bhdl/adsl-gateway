// @flow

'use strict'

import BaseController from '../../../core/socket/server/base.controller'
import rfidEventHandler from '../eventhandlers/rfid.eventhandler'

const route = 'rfid'

const middlewares = []

const eventHandlers = [rfidEventHandler]

export default new BaseController(route, middlewares, eventHandlers)
