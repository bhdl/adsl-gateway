// @flow

'use strict'

import debug from '../../../core/loggers/debug'
import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import ipsStorage from '../../../storages/ips'
import ipsClient from '../controllers/ips.client.controller'

/**
 * Connect Event Handler for Socket.io Client
 * @param {Object} socket
 * @param {Object} namespace
 */
function ipsConnectEventHandler (socket: Object, namespace?: Object) {
  return () => {
    let sendStoredMessages = () => {
      ipsStorage.getAll(failedMessages => {
        debug.info('failedMessages', failedMessages)
        if (!failedMessages || !failedMessages.length) {
          return
        }

        ipsClient.sendAll(failedMessages)
        debug.dev(`Client: connected status is ${socket.connected}`)
        if (socket.connected && failedMessages.length === parseInt(process.env.MONGO_RETRIEVE_LIMIT)) {
          setTimeout(() => {
            sendStoredMessages()
          }, 1000)
        }
      })
    }

    sendStoredMessages()
  }
}

export default new BaseEventHandler('connect', ipsConnectEventHandler)
