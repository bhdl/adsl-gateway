'use strict'

import BaseStorage from './mongo.storage'
import Raven from '../core/loggers/sentry'
import getConfig from '../config/get.config'
import ConfigtypeEnum from '../config/configtype.enum'

export default class MongoGatewayStorage extends BaseStorage {
  getSome (limit = getConfig('MONGO_RETRIEVE_LIMIT', ConfigtypeEnum.INT)) {
    return new Promise((resolve, reject) => {
      this._db.getConnection()
        .then(connection => {
          this._getSome(connection, limit).then(result => {
            resolve(result)
          }).catch(error => { reject(error) })
        }).catch(error => { reject(error) })
    })
  }

  _getSome (connection, limit) {
    return new Promise((resolve, reject) => {
      connection.collection(this._collection)
        .find()
        .project({ _id: 0 })
        .limit(limit)
        .toArray((error, result) => {
          if (error) {
            Raven.captureException(error)
            return reject(error)
          }
          return resolve(result)
        })
    })
  }

  deletePackage (packageId) {
    return new Promise((resolve, reject) => {
      this._db.getConnection()
        .then(connection => { this._deletePackage(connection, packageId) })
        .then(result => { resolve(result) })
        .catch(error => { reject(error) })
    })
  }

  _deletePackage (connection, packageId) {
    return new Promise((resolve, reject) => {
      connection.deleteOne({ packageId: packageId }, (error, result) => {
        if (error) {
          Raven.captureException(error)
          return reject(error)
        }

        return resolve(result)
      })
    })
  }
}
