'use strict'

import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import clientResponseData from '../../../data/client/client.response'
import logger from '../../../core/loggers/logger'
import ipsStorage from '../../../storages/ips'

function ipsEventHandler (socket, namespace) {
  return data => {
    logger.info('incoming-ips-cloud-response', data)
    const errorResponse = clientResponseData.validate(data)

    if (errorResponse) {
      return logger.info('invalid message format')
    }

    ipsStorage.delete(data.packageId)
    // on success remove from storage
    // on error, if wrong data, delete
  }
}

export default new BaseEventHandler('response', ipsEventHandler)
