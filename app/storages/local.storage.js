'use strict'

class LocalStorage extends Map {
  toArray () {
    let dataArray = []
    this.forEach(value => {
      dataArray.push(value)
    })

    return dataArray
  }
}

module.exports = LocalStorage
