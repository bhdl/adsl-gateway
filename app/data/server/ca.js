'use strict'

import Joi from 'joi'

import commonSchema from '../common'
import Validator from '../../core/models/validator'

const emergencySchema = Joi.object({
  distance: Joi.number().required(),
  accuracy: Joi.number().required(),
  sharers: Joi.array().length(2).items(Joi.string()).required(),
})

const dataPackageSchema = Joi.object({
  emergencies: Joi.array().min(1).items(emergencySchema).required()
})

const caSchema = Joi.object({
  ...commonSchema,
  data: dataPackageSchema
})

export default new Validator(caSchema)
