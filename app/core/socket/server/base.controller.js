// @flow

'use strict'

import BaseEventHandler from '../eventhandler/base.eventhandler'
import EventHandlerList from '../eventhandler/eventhandler.list'
import type { middlewareType } from './base.middleware'
import baseErrorEventHandler from '../eventhandler/base.error.eventhandler'
import baseDisconnectingEventHandler from '../eventhandler/base.disconnect.eventhandler'
import baseDisconnectEventHandler from '../eventhandler/base.disconnecting.eventhandler'
import debug from '../../loggers/debug'

export type routeType = string | RegExp

export default class BaseController {
  _route: routeType
  _eventHandlers: EventHandlerList
  _middlewares: Array<middlewareType>

  constructor (route: routeType, middlewares: Array<middlewareType>, eventHandlers: Array<BaseEventHandler>) {
    this.route = route
    this.middlewares = middlewares
    this.eventHandlers = eventHandlers
  }

  set route (route: routeType): void {
    this._route = route
  }

  get route (): routeType {
    return this._route
  }

  set eventHandlers (eventhandlers: Array<BaseEventHandler>): void {
    this._eventHandlers = new EventHandlerList(eventhandlers)
  }

  get eventHandlers (): EventHandlerList {
    return this._eventHandlers
  }

  set middlewares (middlewares: Array<middlewareType>) {
    this._middlewares = []
    for (const middleware of middlewares) {
      this._middlewares.push(middleware)
    }
  }

  get middlewares (): Array<middlewareType> {
    return this._middlewares
  }

  static initController (io: Object, controller: BaseController): void {
    if (controller.route instanceof RegExp) {
      BaseController.initDynamicController(io, controller)
    } else {
      BaseController.initStaticController(io, controller)
    }
  }

  static initDynamicController (io: Object, controller: BaseController): void {
    debug.dev(`Listening incoming data on dynamic route (${controller.route})..`)
    const namespace = io.of(controller.route).on('connect', (socket) => {
      const subNamespace = socket.nsp
      debug.info(`${controller.route} client connected to dynamic route`)
      BaseController.applyEventHandlers(socket, subNamespace, controller.eventHandlers)
    })

    BaseController.applyMiddlewares(namespace, controller.middlewares)
  }

  static initStaticController (io: Object, controller: BaseController) {
    debug.dev(`Listening incoming data on static route (${controller.route})..`)
    const namespace = io.of(controller.route).on('connect', (socket) => {
      debug.info(`${controller.route} client connected to static route`)
      BaseController.applyEventHandlers(socket, namespace, controller.eventHandlers)
    })

    BaseController.applyMiddlewares(namespace, controller.middlewares)
  }

  static applyMiddlewares (namespace: Object, middlewares: Array<middlewareType>): void {
    for (const middleware of middlewares) {
      namespace.use(middleware)
    }
  }

  static applyEventHandlers (socket: Object, namespace: Object, eventHandlers: EventHandlerList) {
    for (const eventHandlerData of eventHandlers) {
      const eventHandler = eventHandlerData[1]
      socket.on(eventHandler.eventName, eventHandler.initEventHandler(socket, namespace))
    }

    const defaultEventHandlers = [baseErrorEventHandler, baseDisconnectingEventHandler, baseDisconnectEventHandler]
    for (const eventHandler of defaultEventHandlers) {
      if (!eventHandlers.hasEventHandler(eventHandler.eventName)) {
        socket.on(eventHandler.eventName, eventHandler.initEventHandler(socket, namespace))
      }
    }
  }
}
