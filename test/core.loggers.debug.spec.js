'use strict'

import { expect } from 'chai'

import debug from '../app/core/loggers/debug'

describe('Debug Module', function () {
  const levels = ['dev', 'info', 'warn', 'error']
  levels.forEach(function (level) {
    it(`should have ${level} method`, function () {
      expect(debug).to.have.property(level)
    })
  })
})
