'use strict'

import BaseClientController from './core/socket/client/base.client.controller'
import ipsClientController from './socket/client/controllers/ips.client.controller'
import rfidClientController from './socket/client/controllers/rfid.client.controller'

function createSocketClients () {
  BaseClientController.initController(ipsClientController)
  BaseClientController.initController(rfidClientController)
}

export default createSocketClients
