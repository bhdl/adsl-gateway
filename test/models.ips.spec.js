'use strict'

import { expect } from 'chai'

import { transformIpsData } from '../app/models/ips'

describe('IPS model transform', function () {
  it('should be correct', function () {
    const testData = {
      timestamp: 1535459539539,
      clientId: 'ips01',
      type: 'position',
      packageId: 'ips01_2018_08_21_10_43_21_123',
      data: {
        positions: [
          {
            tagId: '1',
            timestamp: 1535459410410,
            relativeLocation: {
              x: 1.6,
              y: 12.45,
              z: 2.4,
              accurancy: 0.12
            },
            geolocation: {
              latitude: 1.23455,
              longitude: 1.23344
            }
          },
          {
            tagId: '2',
            timestamp: 1535459310310,
            relativeLocation: {
              x: 1.3,
              y: 13.5,
              z: 3.4,
              accurancy: 0.15
            },
            geolocation: {
              latitude: 1.23444,
              longitude: 1.23444
            }
          },
          {
            tagId: '3',
            timestamp: 1535459510510,
            relativeLocation: {
              x: 2.8,
              y: 20.6,
              z: 1.8,
              accurancy: 0.13
            },
            geolocation: {
              latitude: 1.23555,
              longitude: 1.23555
            }
          }
        ]
      }
    }

    const transformData = transformIpsData(testData)
    for (let i = 0; i < testData.data.positions.length; i++) {
      const exceptedData = testData.data.positions[i]
      expect(transformData[i].packageId).to.be.equal(testData.packageId)
      expect(transformData[i].clientId).to.be.equal(testData.clientId)
      expect(transformData[i].tagId).to.be.equal(exceptedData.tagId)
      expect(transformData[i].relativeLocation.x).to.be.equal(exceptedData.relativeLocation.x)
      expect(transformData[i].relativeLocation.y).to.be.equal(exceptedData.relativeLocation.y)
      expect(transformData[i].relativeLocation.z).to.be.equal(exceptedData.relativeLocation.z)
      expect(transformData[i].relativeLocation.accuracy).to.be.equal(exceptedData.relativeLocation.accuracy)
      expect(transformData[i].geolocation.latitude).to.be.equal(exceptedData.geolocation.latitude)
      expect(transformData[i].geolocation.longitude).to.be.equal(exceptedData.geolocation.longitude)
    }
  })
})
