// @flow

'use strict'

import BaseClientController from '../../../core/socket/client/base.client.controller'
import ipsEventHandler from '../eventhandlers/ips.response.client.eventhandler'
import ipsConnectEventHandler from '../eventhandlers/ips.connect.client.eventhandler'
import debug from '../../../core/loggers/debug'

const route = 'gateways/srth-1/ips'

const middlewares = []

const eventHandlers = [ipsEventHandler, ipsConnectEventHandler]

const ipsClientController = new BaseClientController(route, middlewares, eventHandlers)

ipsClientController.on('messageSent', (message) => {
  debug.info(`IPS Message sent to Cloud ${message.packageId}`)
})

export default ipsClientController
