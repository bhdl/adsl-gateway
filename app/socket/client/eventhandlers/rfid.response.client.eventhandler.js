'use strict'

import BaseEventHandler from '../../../core/socket/eventhandler/base.eventhandler'
import clientResponseData from '../../../data/client/client.response'
import logger from '../../../core/loggers/logger'
import debug from '../../../core/loggers/debug'
import rfidStorage from '../../../storages/rfid'

function rfidEventHandler (socket, namespace) {
  return data => {
    logger.info('incoming-rfid-cloud-response', data)
    const errorResponse = clientResponseData.validate(data)

    if (errorResponse) {
      debug(errorResponse.getResponseData())
      return logger.info('invalid message format', errorResponse.getResponseData())
    }

    rfidStorage.delete(data.packageId)
    // on success remove from storage
    // on error, if wrong data, delete
  }
}

export default new BaseEventHandler('response', rfidEventHandler)
