'use strict'

import ioServer from 'socket.io'

import BaseController from './core/socket/server/base.controller'
import ipsController from './socket/server/controllers/ips.controller'
import rfidController from './socket/server/controllers/rfid.controller'
import caController from './socket/server/controllers/ca.controller'

function createSocket (server) {
  const io = ioServer(server)
  BaseController.initController(io, ipsController)
  BaseController.initController(io, rfidController)
  BaseController.initController(io, caController)
  return io
}

export default createSocket
