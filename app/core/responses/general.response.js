// @flow

'use strict'

import { formatConfig } from '../../config'

export type statusType = 'OK' | 'ERROR' | 'INFO'

class GeneralResponse {
  _status: statusType
  _code: number
  _message: string
  _packageId: string
  _data: Object

  constructor (status: statusType, code: number, message: string, packageId: string, data?: Object) {
    this.status = status
    this.code = code
    this.message = message
    this.packageId = packageId
    this.data = data || {}
  }

  getResponseData (): Object {
    return {
      status: this.status,
      code: this.code,
      message: this.message,
      packageId: this.packageId,
      data: this.data
    }
  }

  set status (value: statusType) {
    this._status = value
  }

  get status (): statusType {
    return this._status
  }

  set code (value: number) {
    const statusCodeMinValue = this.minStatusCode
    const statusCodeMaxValue = this.maxStatusCode

    if (isNaN(value) || value === Infinity) {
      throw new TypeError('Error code should be a valid number!')
    }

    if (value < statusCodeMinValue || value >= statusCodeMaxValue) {
      throw new Error(`Response code should be between ${statusCodeMinValue} and ${statusCodeMaxValue}!`)
    }

    this._code = parseInt(value)
  }

  get code (): number {
    return this._code
  }

  set message (value: string) {
    this._message = value
  }

  get message (): string {
    return this._message
  }

  set packageId (value: string) {
    const requiredFormat = formatConfig.packageId

    if (!requiredFormat.test(value)) {
      throw new Error('The package id format is not valid!')
    }

    this._packageId = value
  }

  get packageId () {
    return this._packageId
  }

  set data (value: Object) {
    this._data = value
  }

  get data (): Object {
    return this._data
  }

  get minStatusCode (): number {
    return 100
  }

  get maxStatusCode (): number {
    return 600
  }
}

export default GeneralResponse
